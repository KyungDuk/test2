
var winSize;
var main;
var touchListener;
var level = 1;
var life = 3;
var score = 0;
var maxLevel = 10;
var arrLife = new Array();
var lbLevel;
var lbScore;
var lbLevelTitle;
var language = "kr/";
var resourcesLDir = "./Resources/" + language + "part1/";
var resourcesDir = "./Resources/";
var touchState = false;
var popupLayer;
var gauge;
var timer = 0;
var maxTime = 5 * 10;
var sprGhost = null;
var catchGhostCount = 0;
var totalCatchGhostCount = 0;
var isFinish = false;
var isPause = false;
var scrollView;
var resize = 2;
var scrollViewWidth = 347*resize;
var kakaoTalkKey = "7f95e8025b0d8fb11f65eb9f3dd485e7";
var touchPoint;
var snsLink = "http://13.231.222.238/ScreenerMain.html";
var lifeAction;


window.onload = function(){
    
    cc.game.onStart = function(){
    
        cc.director.setProjection(cc.Director.PROJECTION_3D);
        var policy = new cc.ResolutionPolicy(cc.ContainerStrategy.PROPORTION_TO_FRAME, cc.ContentStrategy.SHOW_ALL);
        //cc.view.setDesignResolutionSize(375, 667, policy);
        cc.view.setDesignResolutionSize(750, 1334, policy);

        cc.view.resizeWithBrowserSize(true);
        cc.view.enableAutoFullScreen(true);
        cc.director.setDisplayStats(false);
        cc._loaderImage = resourcesLDir + "startbg.png";

        cc.LoaderScene.preload([resourcesLDir + "startbg.png"], function () {

            var MyScene = cc.Scene.extend({

                onRestart:function (){
                    cc.director.setDisplayStats(false);
                },

                onEnter:function () {
                    this._super();
                    cc.view.enableAutoFullScreen(true);
                    
                    winSize = cc.director.getWinSize();
                    main = this;

                    initUI();   
                    initStartPopup();
                    initCopyright();

                    touchListener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: false,

                    onTouchBegan: function (touch, event) {	

                        if (!touchState || isPause) return true;
 
                        var target = event.getCurrentTarget();
                        var location = target.convertToNodeSpace(touch.getLocation());
                        touchPoint = location;
                        var click = false;

                        var rect = cc.rect(sprGhost.getPositionX() - sprGhost.width/2, sprGhost.getPositionY()- sprGhost.height/2, sprGhost.width, sprGhost.height);

                        if (cc.rectContainsPoint(rect, location)){
                            click = true;
                        } 

                        checkGameFinish(click); 
                        
                        return true;
                    },
                    onTouchMoved: function (touch, event) {			


                        return true;
                    },
                    onTouchEnded: function (touch, event) {	
                        
                        
                    },
                });
                cc.eventManager.addListener(touchListener,this);

                cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function () {
                    
                    cc.director.setDisplayStats(false);
                });

                    
                }
            });
            cc.director.runScene(new MyScene());
        }, this);
        
    };
    cc.game.run("gameCanvas");

    

    function gameStart(){
        touchState = true;
        initObject();
        timer = 0;
        mainGameSchedule();
    } 
    function resetData(){
        timer = 0;
        catchGhostCount = 0;
        
    }
    function setGauge() {
        var newProgress = ((maxTime-timer) * 100 / maxTime) / 100;
        var action = cc.scaleTo(0.05, newProgress, 1);
        gauge.runAction(action);
    }
    function initCopyright(){
        var lbCopyright = cc.LabelTTF.create("Copyright ⓒ 2020, Johnson & Johnson Korea Ltd, All rights reserved.", "Helvetica", 20);
        lbCopyright.setPosition(cc.p(winSize.width/2,45));
        lbCopyright.setColor(cc.color(255, 255, 255));
        popupLayer.addChild(lbCopyright);

        var lbCcp = cc.LabelTTF.create("KR_2019_469", "Helvetica", 20);
        lbCcp.setPosition(cc.p(winSize.width/2 + 260,20));
        lbCcp.setColor(cc.color(255, 255, 255));
        main.addChild(lbCcp,2);
    }
    /**
     * 기본 UI 구성
     * */
    function initUI(){

        var mainBG = cc.Sprite.create(resourcesDir + "game_bg.png");
        mainBG.setPosition(cc.p(winSize.width/2, winSize.height/2));
        main.addChild(mainBG);

        // 하트 
        for (var i = 0; i < 3; i++) {
            var temp = 30*resize;
            var sprLife = cc.Sprite.create(resourcesDir + "life.png");
            sprLife.setPosition(cc.p(45*resize + (temp * i),  winSize.height - 32*resize));
            sprLife.setTag(i);
            main.addChild(sprLife);

            arrLife.push(sprLife);
        }
        
        // 게이지
        gauge = cc.Sprite.create(resourcesDir + "time_full.png");
        gauge.setPosition(cc.p(31*resize, winSize.height - 60*resize));
        gauge.setAnchorPoint(0,0.5);
        gauge.setScale(0.98);
        main.addChild(gauge);

        var gaugeBG = cc.Sprite.create(resourcesDir + "timebar.png");
        gaugeBG.setPosition(cc.p(30*resize, winSize.height - 60*resize));
        gaugeBG.setAnchorPoint(0,0.5);
        main.addChild(gaugeBG);

        lbLevelTitle = cc.LabelTTF.create("Level", "Noto Sans KR", 40);
        lbLevelTitle.setPosition(cc.p(winSize.width - 90*resize,winSize.height - 33*resize));
        lbLevelTitle.setColor(cc.color(127, 0, 216));
        main.addChild(lbLevelTitle);

        lbLevel = cc.LabelTTF.create(level, "Noto Sans KR", 40);
        lbLevel.setPosition(cc.p(72*resize,14*resize));
        lbLevel.setColor(cc.color(127, 0, 216));
        lbLevel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT);
        lbLevelTitle.addChild(lbLevel);

        lbScore = cc.LabelTTF.create("0", "Noto Sans KR", 40);
        lbScore.setPosition(cc.p(winSize.width - 77*resize , winSize.height - 60*resize));
        lbScore.setColor(cc.color(127, 0, 216));
        main.addChild(lbScore);

        var sprFail = cc.Sprite.create(resourcesDir + "touch_x.png");
        sprFail.setPosition(cc.p(winSize.width/2, winSize.height/2));
        main.addChild(sprFail);
        sprFail.setTag(77);
        sprFail.setVisible(false);

        var sprLifeEffect = cc.Sprite.create(resourcesDir + "life1.png");
        sprLifeEffect.setPosition(cc.p(winSize.width/2, winSize.height/2));
        main.addChild(sprLifeEffect);
        sprLifeEffect.setTag(78);
        sprLifeEffect.setVisible(false);
        sprLifeEffect.setScale(0.45);

        var animFrames = new Array();
        for (var i = 1; i <= 6; i++) {
           var frame = cc.SpriteFrame.create(resourcesDir + "life" + i + ".png", cc.rect(0, 0, 99, 44));
           animFrames.push(frame);
        }

        var animation = cc.Animation.create(animFrames);
        animation.setDelayPerUnit(0.05);
        lifeAction = cc.Animate.create(animation);

        var lbLife = cc.LabelTTF.create("-1", "Noto Sans KR", 20);
        lbLife.setPosition(cc.p(96 , 20));
        lbLife.setColor(cc.color(0, 0, 0));
        sprLifeEffect.addChild(lbLife);
        lbLife.setTag(79);
        lbLife.setScale(2.0);

    }
    
    /**
     * 오브젝트 생성
     * */
    function initObject(){

        if (sprGhost == null) {
            var random = Math.floor(Math.random() * 8) + 1;
            var randomPosition = getRandomPosition();

            sprGhost = cc.Sprite.create(resourcesDir + "mainghost" + random + ".png");
            sprGhost.setPosition(cc.p(randomPosition.width, randomPosition.height));
            sprGhost.setOpacity(getOpacity(),1,100);
            sprGhost.setTag(random-1);
            main.addChild(sprGhost);
        }
        
    }
    function removeGhost(){
        if (sprGhost != null) {
            main.removeChild(sprGhost);
            sprGhost = null;
        }
    }

    function getRandomPosition(){

        var revision = 50*resize;
        var randomX = Math.floor(Math.random() * (260*resize)) + 1 + revision;
        var randomY = Math.floor(Math.random() * (310*resize)) + 1 + 220*resize;
        var size = cc.size(randomX,randomY); 

        return size;
    }

    /**
     *  타이머 스케쥴 
     * */
    function mainGameSchedule() {

        if (isFinish || isPause) return;

        timer++;

        if (timer > 50) {
            checkLife();

            if (life > 0) {
                timer = 0;
  
                setTimeout(mainGameSchedule,100);
            } 
        } else {
            setGauge();
            setTimeout(mainGameSchedule,100);
        }
    }
    function checkGameFinish(isTouch){
        // 오브젝트 클릭 미스시 life 차감
        if (!isTouch) {
            timer = 0;
            checkLife();
            initFailAnimation();
        } else {
            // catchGhostCount가 5개이상일때 다음 스탭 으로 진행.
            catchGhostCount++;
            setScore();
            if (catchGhostCount >= 5) {

                if (level >= maxLevel) {
                    // 게임 완료
                    initResultPopup(true);
                } else {
                    // 데이터 초기화  
                    resetData();
                    // 레벨 상승  
                    setLevel();
                    
                }
            } 

            // var spawn = cc.Spawn.create(cc.FadeOut.create(0.3), cc.MoveTo.create(0.2, cc.p(sprGhost.getPositionX(), sprGhost.getPositionY() + 30)));
            // var sequence = cc.sequence([
            //     spawn,
            //     cc.callFunc(function () {
            //         removeGhost();
            //         initObject();
            //     }, main)
            // ]);

            var sequence = cc.sequence([
                // cc.FadeOut.create(0.05),
                cc.callFunc(function () {
                    removeGhost();
                    initObject();
                }, main)
            ]);

            sprGhost.runAction(sequence);
        }
    }
    function initFailAnimation(){

        var spr = main.getChildByTag(77);
        var sprLifeEffect = main.getChildByTag(78);
        var lbLife = sprLifeEffect.getChildByTag(79);
        
        if(spr != null){
            
            spr.setVisible(true);
            spr.setPosition(cc.p(touchPoint.x, touchPoint.y));
            
            lbLife.setVisible(true);
            lbLife.setOpacity(255);

            sprLifeEffect.setVisible(true);
            sprLifeEffect.setOpacity(255);
            sprLifeEffect.setPosition(cc.p(touchPoint.x + 55, touchPoint.y + 17));

            var scaleUp = cc.scaleTo(0.2, 2.0, 2.0);
            var scaleDown = cc.scaleTo(0.2, 1.0, 1.0);
            var sequence = cc.sequence([
                // scaleUp,
                // scaleDown,
                cc.FadeOut.create(1.2),
                cc.callFunc(function () {
                    spr.setOpacity(255);
                    spr.setVisible(false);    
                }, main)
            ]);

            spr.runAction(sequence);

            var calfunc = cc.callFunc(function () {
                            lbLife.runAction(cc.FadeOut.create(0.5));
                          }, main)
            var seq = cc.sequence([
                //spawn,
                cc.MoveTo.create(0.5, cc.p(touchPoint.x + 55, touchPoint.y + 17 + 30)),
                cc.Spawn.create(cc.FadeOut.create(0.5), calfunc)
            ]);
            sprLifeEffect.runAction(seq);

            //lifeAction
        }
    }
    function nextStepPause(){
        isPause = true;

        var sprLevel = cc.Sprite.create(resourcesDir + "level" + level + ".png");
        sprLevel.setPosition(cc.p(winSize.width/2, winSize.height/2));
        main.addChild(sprLevel);
    }
    /**
     * Score 
     * */
    function setScore(){

        score += level * 1000;
        totalCatchGhostCount++;
        lbScore.setString(comma(score));
    }
    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    /**
     * Life check
     * */
    function checkLife(){
        
        life--;

        for (var i = 0; i < arrLife.length; i++) {
            var aprLife = arrLife[i];

            if (aprLife.getTag() == life) {
                aprLife.setOpacity(getOpacityObj(30));
                break;
            }
        }

        if (life <= 0) initResultPopup(false);
    }
    
    /**
     * 밝기 값
     * */
    function getOpacity(){

        var value = 100;
        switch (level) {
            case 1 : value = 30; break;//30  70
            case 2 : value = 10; break;//10  50
            case 3 : value = 5; break;//5   40
            case 4 : value = 3; break;//3   30
            case 5 : value = 1; break;//1   15
            case 6 : value = 1; break;//10
            case 7 : value = 1; break;//5
            case 8 : value = 1; break;//3
            case 9 : value = 1; break;//2 
            case 10 :value = 1; break;//1 

            default:
            break;
        }

        var opacity = 255 / 100 * value; 

        return opacity;
    }
    function getOpacityObj(value){
        var opacity = 255 / 100 * value; 
        return opacity;
    }
    /**
     * Level 변경
     * */
    function setLevel() {
        level += 1;
        lbLevel.setString(level);

        var scaleUp = cc.scaleTo(0.2, 1.2, 1.2);
        var scaleDown = cc.scaleTo(0.2, 1.0, 1.0);

        var sequence = cc.sequence([
            scaleUp,
            scaleDown
            ]);
        lbLevelTitle.runAction(sequence);
     }

     /**
     * 스타트 팝업
     * */
    function initStartPopup() {

        popupLayer = new cc.LayerColor(cc.color(255,255,255,255));
        popupLayer.setPosition(cc.p(0 /2,0));
        main.addChild(popupLayer,2);

        var startImage = new cc.MenuItemImage(resourcesLDir + "startbg.png",resourcesLDir + "startbg.png", startFunc); //main.png startbg
        var menuStart = new cc.Menu(startImage);
        menuStart.setPosition(cc.p(winSize.width/2,winSize.height/2));
        popupLayer.addChild(menuStart);   

        var sprTouch = cc.Sprite.create(resourcesDir + "touch.png");
        sprTouch.setPosition(cc.p(winSize.width/2, winSize.height/2 - 240*resize));
        popupLayer.addChild(sprTouch);
        sprTouch.setOpacity(0);

        var sequence = cc.sequence([
            cc.FadeIn.create(1.0),
            cc.FadeOut.create(1.0)
            ]).repeatForever();
        
        sprTouch.runAction(sequence);
        initBat();
        initGhost();

     }
     function initBat(){
        
        var position = [ [55*resize,winSize.height - 280], [winSize.width - 60*resize,  winSize.height - 140] , [70*resize,  winSize.height - 100] , [winSize.width - 80*resize,  winSize.height - 290]];
        var delayTime = [0.2 , 0.25 , 0.2 , 0.25];
        var scale = [1.0, 0.7, 0.4, 0.3];
        var opacity = [100, 90, 40, 30];
        var rotate = [0, 10, 340, 350];
        var arrSpr = new Array();   

        for (var i = 0; i < 4; i++) {
            var spr = cc.Sprite.create(resourcesDir + "bat1.png");
            spr.setPosition(cc.p(position[i][0],position[i][1]));
            spr.setScale(scale[i]);
            spr.setOpacity(getOpacityObj(opacity[i]));
            spr.setRotation(rotate[i]);
            popupLayer.addChild(spr);

            if (i == 2)  spr.setFlippedX(true);
            if (i == 3)  spr.setFlippedX(true);

            arrSpr.push(spr);
        }

        var animFrames = new Array();
        for (var i = 1; i <= 8; i++) {
           var frame = cc.SpriteFrame.create(resourcesDir + "bat" + i + ".png", cc.rect(0, 0, 168, 88));
           animFrames.push(frame);
        }

        for (var i = 0; i < arrSpr.length; i++) {
            var animation = cc.Animation.create(animFrames);
            animation.setDelayPerUnit(delayTime[i]);
            var action = cc.Animate.create(animation);
            var repeat = cc.RepeatForever.create(action);
            arrSpr[i].runAction(repeat); 
        }
     }
     function initGhost(){

        var position = [ [winSize.width/2 + 72*resize,winSize.height/2 - 87*resize], [winSize.width/2 - 80,winSize.height/2 - 81*resize], [winSize.width/2 + 12 ,winSize.height/2 - 68*resize]];
        var opacity = [100, 80, 60];
        var scale = [0.7, 0.6, 0.3];
        var rotate = [0, 30, -30];
        var delayTime = [1,1.5,3];
        var arrSpr = new Array();

        for (var i = 0; i < 3; i++) {
            var spr = cc.Sprite.create(resourcesDir + "ghost1.png");
            spr.setPosition(cc.p(position[i][0],position[i][1]));
            spr.setScale(scale[i]);
            spr.setOpacity(getOpacityObj(opacity[i]));
            spr.setRotation(rotate[i]);
            spr.setVisible(false);
            popupLayer.addChild(spr);
            arrSpr.push(spr);

            if (i == 1)  spr.setFlippedX(true);
        }

        var animFrames = new Array();
        for (var i = 1; i <= 6; i++) {
           var frame = cc.SpriteFrame.create(resourcesDir + "ghost" + i + ".png", cc.rect(0, 0, 220, 180));
           animFrames.push(frame);
        }

        for (var i = 0; i < arrSpr.length; i++) {
            var animation = cc.Animation.create(animFrames);
            animation.setDelayPerUnit(0.2);
            var action = cc.Animate.create(animation);
            
            var sequence = cc.sequence([
                           cc.delayTime(delayTime[i]),
                           cc.callFunc(function (spr) {
                            spr.setVisible(true);
                            }, main),
                           action]).repeatForever();

            arrSpr[i].runAction(sequence); 
        }

     }

     var startFunc = function() {

        main.removeChild(popupLayer);
        initStartGuidePopup();
    }
    /**
     *  타이머 팝업
     * */
    function initStartGuidePopup(){
       
        popupLayer = new cc.LayerColor(cc.color(0,0,0,100));
        popupLayer.setPosition(cc.p(0,0));
        main.addChild(popupLayer,2);

        var sprBG = cc.Sprite.create(resourcesLDir + "startpopupguide.png");
        sprBG.setPosition(cc.p(winSize.width/2, winSize.height/2));
        popupLayer.addChild(sprBG);
        sprBG.setTag(10);

        var sprCount = cc.Sprite.create(resourcesDir + "count3.png");
        sprCount.setPosition(cc.p(winSize.width/2 - 26*resize, 55*resize));
        sprBG.addChild(sprCount);
        sprCount.setTag(1);

        var startImage = new cc.MenuItemImage(resourcesDir + "start.png",resourcesDir + "start.png", gameFunc);
        startImage.setPosition(cc.p(187*resize,232*resize));
        var menuStart = new cc.Menu(startImage);
        menuStart.setPosition(cc.p(0,0));
        popupLayer.addChild(menuStart);  
        menuStart.setTag(11);

    }
    function gameFunc(){

        var menuStart = popupLayer.getChildByTag(11);
        if (menuStart != null) {
            menuStart.setVisible(false);
        }
        timer = 3;
        setTimeout(startGuideSchedule,1000);
    }
    /**
     *  타이머 스케쥴 
     * */
    function startGuideSchedule(){
        
        timer--;

        if (timer < 1) {
            touchState = true;
            main.removeChild(popupLayer);
            gameStart();

        } else {
        var sprBG = popupLayer.getChildByTag(10);
        
        if (sprBG != null) {
            
            var sprCount = sprBG.getChildByTag(1);
            if (sprCount != null) {
                sprCount.setTexture(resourcesDir + "count" + timer + ".png");
                setTimeout(startGuideSchedule,1000);
            }
        }
        }
    }
    /**
     * 종료 팝업
     * */
    function initResultPopup(isClear) {
        touchState = false;
        isFinish = true;
        var clear = (isClear == true)? 1 : 0;

        if (!isClear) {
             findGhost(clear);
        } else {
            
            var sequence = cc.sequence([
                cc.delayTime(1.0),
                cc.callFunc(function () { 
                    initGameOver(clear);
                }, main)
                ]);
    
            lbScore.runAction(sequence);   
 
        }
     }

     function findGhost(isClear){

        var x = [0,-4,-4,0,-4,-10,2,0];
        var y = [-4,-4,-4,-4,-4,-10,-2,-4];
        var index = sprGhost.getTag();
        var sequence = cc.sequence([
            cc.FadeIn.create(2.0),
            cc.callFunc(function () { 
                initGameOver(isClear);
            }, main)
            ]);

        sprGhost.runAction(sequence);   

        var sprEllipse = cc.Sprite.create(resourcesDir + "ellipse.png");
        sprEllipse.setPosition(cc.p(-10*resize + x[index], -10*resize  + y[index]));
        sprEllipse.setAnchorPoint(0,0);
        sprGhost.addChild(sprEllipse); 

     }
     function initGameOver(isClear){

        var fileName = ["game_over.png","game_clear.png"]

        popupLayer = new cc.LayerColor(cc.color(0,0,0,100));
        popupLayer.setPosition(cc.p(0 ,0));
        main.addChild(popupLayer,2);

        var sprfinish = cc.Sprite.create(resourcesDir + fileName[isClear]);
        sprfinish.setPosition(cc.p(winSize.width/2, winSize.height/2 + 30*resize));
        popupLayer.addChild(sprfinish); 

        var sequence = cc.sequence([
            cc.delayTime(3.0),
            cc.callFunc(function () { 
                initScorePopup(isClear);
            }, main)
            ]);

        sprfinish.runAction(sequence);

     }

     function initScorePopup(isClear){

        main.removeChild(popupLayer);

        var backgroundBG = ["resultfail.png", "resultclear.png"];
        var strScore = score;
        var strTotalCatchGhostCount = totalCatchGhostCount;
        var scrollViewHeight = [990, 545*resize];//505, 545
        var y = (isClear == 1)? 5:0;

        if (score == 0) strScore = "0";
        if (totalCatchGhostCount == 0) strTotalCatchGhostCount = "0";

        popupLayer = new cc.LayerColor(cc.color(0,0,0,100));
        popupLayer.setPosition(cc.p(0 ,0));
        main.addChild(popupLayer,2);

        var sprfinish = cc.Sprite.create(resourcesDir + backgroundBG[isClear]);
        sprfinish.setPosition(cc.p(winSize.width/2, winSize.height/2));
        popupLayer.addChild(sprfinish);

        initScrollview(sprfinish, isClear);

        var sprGreenBG = cc.Sprite.create(resourcesDir + "scorebg.png");
        sprGreenBG.setPosition(cc.p(scrollViewWidth/2, 910));
        scrollView.addChild(sprGreenBG);
        
        var sprLevel = cc.Sprite.create(resourcesDir + "level" + level + ".png");
        
        var lbScore = cc.LabelTTF.create(comma(strScore), "Noto Sans KR", 26*2);
        lbScore.setPosition(cc.p(218*resize, 20*resize));
        lbScore.setColor(cc.color(0, 0, 0));
        lbScore.setAnchorPoint(0.5,0.5);
        sprGreenBG.addChild(lbScore);

        var sprLevelContext = cc.Sprite.create(resourcesLDir + "result.png");
        sprLevelContext.setPosition(cc.p(scrollViewWidth/2, 760));
        scrollView.addChild(sprLevelContext);

        var sprResultContext = cc.Sprite.create(resourcesLDir + "resulttext.png");
        sprResultContext.setPosition(cc.p(scrollViewWidth/2, 350));
        scrollView.addChild(sprResultContext);

        // // 실패
        if (isClear == 0) {
           
            sprLevel.setPosition(cc.p(winSize.width/2, winSize.height - 110*resize));
            sprLevel.setScale(0.7);
            sprfinish.addChild(sprLevel);

        } else {
    
            sprLevel.setPosition(cc.p(scrollViewWidth/2 - 26*resize, 75*resize));
            sprLevel.setScale(0.45);
            sprGreenBG.addChild(sprLevel);

          }
    
        var retryImage = new cc.MenuItemImage(resourcesLDir + "replaybtn.png",resourcesLDir + "replaybtn.png", retryCallback);
        retryImage.setPosition(cc.p(54*resize,52*resize - y));
        var retryMenu = new cc.Menu(retryImage);
        retryMenu.setPosition(cc.p(0,0));
        sprfinish.addChild(retryMenu);   

        var nextStepImage = new cc.MenuItemImage(resourcesLDir + "nextstep.png",resourcesLDir + "nextstep.png", nextStepCallback);
        nextStepImage.setPosition(cc.p(188*resize,52*resize- y));
        var nextStepMenu = new cc.Menu(nextStepImage);
        nextStepMenu.setPosition(cc.p(0,0));
        sprfinish.addChild(nextStepMenu);  

        var sociallinkImage = new cc.MenuItemImage(resourcesLDir + "sociallink.png",resourcesLDir + "sociallink.png", socialLinkCallback);
        sociallinkImage.setPosition(cc.p(322*resize,52*resize- y));
        var sociallinkMenu = new cc.Menu(sociallinkImage);
        sociallinkMenu.setPosition(cc.p(0,0));
        sprfinish.addChild(sociallinkMenu); 

        scrollView.setInnerContainerSize(cc.size(scrollViewWidth,scrollViewHeight[isClear]));
        

     }
     function initScrollview(target, isClear){

        var position = [25*resize, 95];
        var size = [815,745];
        var fileName = ["failbg.png","clearbg.png"];

        if (scrollView != null) {
            popupLayer.removeChild(scrollView);
            scrollView = null;
        }

        scrollView = new ccui.ScrollView();
        scrollView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        scrollView.setTouchEnabled(true);
        scrollView.setBounceEnabled(true);
        // scrollView.setInertiaScrollEnabled(true);
        scrollView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        scrollView.setBackGroundColor(cc.color(34,34,34));
        // scrollView.setContentSize(cc.size(scrollViewWidth, size[isClear]));
        scrollView.setContentSize(cc.size(347*resize, size[isClear]));
        scrollView.setAnchorPoint(cc.p(0.5, 0.5));
        scrollView.setBackGroundImage(resourcesDir + fileName[isClear]);
        scrollView.setPosition(cc.p(winSize.width/2, winSize.height / 2 - position[isClear]));
        popupLayer.addChild(scrollView);

     }

     function retryCallback(){

        if (isPause == true) return;
        
        main.removeChild(popupLayer);
        removeGhost();

        level = 1;
        life = 3; 
        score = 0; 
        catchGhostCount = 0;
        totalCatchGhostCount = 0; 
        isFinish = false;
        touchState = false;
        timer = 0;
        catchGhostCount = 0;

        lbLevel.setString(level);
        lbScore.setString(0);
        for (var i = 0; i < arrLife.length; i++) {
            var aprLife = arrLife[i];
            aprLife.setOpacity(getOpacityObj(100));
        }

        var action = cc.scaleTo(0.05, 1, 1);
        gauge.runAction(action);

        initStartGuidePopup();

     }
     function nextStepCallback(){
        
        if (isPause == true) return;
        console.log("nextStepCallback");
        // location.replace(Screener2Main.html);
        // document.getElementById("location_replace").innerHTML = "Screener2Main.html";
        // window.location.replace = './Screener2Main.html';
        window.location.href = "http://13.231.222.238/Screener2Main.html";
     }
     function socialLinkCallback(){
        
        if (isPause == true) return;
        isPause = true;
        scrollView.setTouchEnabled(false);

        var socialLinkLayer = new cc.LayerColor(cc.color(0,0,0,100));
        socialLinkLayer.setPosition(cc.p(0 ,0));
        main.addChild(socialLinkLayer,3);
        socialLinkLayer.setTag(111);

        var sprSocialPopup = cc.Sprite.create(resourcesLDir + "share.png");
        sprSocialPopup.setPosition(cc.p(winSize.width/2, winSize.height/2 + 80));
        socialLinkLayer.addChild(sprSocialPopup);

        var kakaoImage = new cc.MenuItemImage(resourcesDir + "kakao.png",resourcesDir + "kakao.png", kakaoCallback);
        var kakaoMenu = new cc.Menu(kakaoImage);
        kakaoMenu.setPosition(cc.p(winSize.width/2,268*resize + 80));
        socialLinkLayer.addChild(kakaoMenu);   

        var facebookImage = new cc.MenuItemImage(resourcesDir + "fb.png",resourcesDir + "fb.png", facebookCallback);
        var facebookMenu = new cc.Menu(facebookImage);
        facebookMenu.setPosition(cc.p(winSize.width/2,210*resize + 80));
        socialLinkLayer.addChild(facebookMenu); 

        var closeImage = new cc.MenuItemImage(resourcesDir + "x.png",resourcesDir + "x.png", snsCloseCallback);
        var closeMenu = new cc.Menu(closeImage);
        closeMenu.setPosition(cc.p(winSize.width/2 + 118*resize,365*resize + 80));
        socialLinkLayer.addChild(closeMenu); 

     }
     function kakaoCallback(){
        
        var linkURL = snsLink;
        Kakao.init(kakaoTalkKey);
        Kakao.Link.sendDefault({
            objectType: 'text',
            text: '유령을 잡아라!',
            installTalk: true,
            link: {
                webUrl: linkURL,
                mobileWebUrl: linkURL
            },
            buttons: [{
            title: '자세히 보기',
            link: {
                webUrl: linkURL,
                mobileWebUrl: linkURL
            }
            }],
        callback: function(data) {
             // 추천이력 등록
             //sendRecommendSNS("kakao");
        }
        });
     }
     function facebookCallback(){
        // var url = "http://www.naver.com";
        // window.open('http://facebook.com/sharer/sharer.php?u='+encodeURIComponent(url), '', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');

        FB.ui({
            method: 'share',
            href: snsLink
          }, function(response){});
        
     }
     function snsCloseCallback(){
        
        var socialLinkLayer = main.getChildByTag(111);

        if (socialLinkLayer != null) {
            main.removeChild(socialLinkLayer);
            socialLinkLayer = null;
            isPause = false;
            scrollView.setTouchEnabled(true);
        }

     }
};

