
var winSize;
var main;
var touchListener;
var language = "kr/"
var resourcesLDir = "./Resources/" + language + "part2/";
var resourcesDir = "./Resources/";
var touchState = false;
var arrowState = false;
var isSwipe = false;
var popupLayer;
var guideLayer;
var sprRoad;
var bg;
var arrow;
var isArrow = false;
var targetPositionY;
var targetPositionX;
var step = 1;
var touchPoint;
var arrRolling = new Array();
var arrBtn = new Array();
var arrResult = new Array();
var sprContextbg;
var sprContext;
var lbAngel;
var sprBg;
var scrollView;
var angleResult = 0;
var isPause = false;
var kakaoTalkKey = "7f95e8025b0d8fb11f65eb9f3dd485e7";
var snsLink = "http://13.231.222.238/Screener2Main.html";


window.onload = function(){
    
    cc.game.onStart = function(){
        
        cc.director.setProjection(cc.Director.PROJECTION_3D);
        var policy = new cc.ResolutionPolicy(cc.ContainerStrategy.PROPORTION_TO_FRAME, cc.ContentStrategy.SHOW_ALL);
        cc.view.setDesignResolutionSize(750, 1334, policy);
        cc.view.resizeWithBrowserSize(true);
        cc.view.enableAutoFullScreen(true);
        cc.director.setDisplayStats(false);

        cc._loaderImage = resourcesLDir + "part2bg.png";

        cc.LoaderScene.preload([resourcesLDir + "part2bg.png"], function () {

            var MyScene = cc.Scene.extend({
                onEnter:function () {
                    
                    this._super();
                    cc.view.enableAutoFullScreen(true);
                    
                    winSize = cc.director.getWinSize();
                    main = this;
                    var mainLayer = new cc.LayerColor(cc.color(2,4,63,255));
                    mainLayer.setPosition(cc.p(0,0));
                    main.addChild(mainLayer);

                    initGuideUI();   
                    initStartPopup();
                    initCopyright();

                    touchListener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: false,

                    onTouchBegan: function (touch, event) {	

                        if (touchState == false) return;

                        setArrowAngle(touch, event);

                        var target = event.getCurrentTarget();
                        var location = target.convertToNodeSpace(touch.getLocation());
                        touchPoint = location; 
        
                        return true;
                    },
                    onTouchMoved: function (touch, event) {			

                        if (touchState == false) return;
                        setArrowAngle(touch, event);
                        
                        return true;
                    },
                    onTouchEnded: function (touch, event) {	
                        
                        if (touchState == false) return;
                        if (isSwipe == true) return;

                        var target = event.getCurrentTarget();
                        var location = target.convertToNodeSpace(touch.getLocation());

                        if (touchPoint.x < location.x){
                            setSwipe(true);
                        } else {
                            setSwipe(false);
                        }
                        
                    },
                });
                cc.eventManager.addListener(touchListener,this);
                cc.eventManager.addCustomListener(cc.game.EVENT_SHOW, function () {
                    cc.director.setDisplayStats(false);
                });
                }
            });
            cc.director.runScene(new MyScene());
        }, this);
    };
    cc.game.run("gameCanvas");

    
    /**
     * 기본 UI 구성
     * */
    function initGuideUI(){

        guideLayer = new cc.LayerColor(cc.color(2,4,63,255));
        guideLayer.setPosition(cc.p(0,0));
        main.addChild(guideLayer);

        var sprMan = cc.Sprite.create(resourcesDir + "man_1.png");
        sprMan.setPosition(cc.p(winSize.width/2,  winSize.height - 400));
        guideLayer.addChild(sprMan);
        sprMan.setTag(20);

        var temp = 0;
        for (var i = 0; i < 3; i++){
            temp += 55;
            var sprRolling = cc.Sprite.create(resourcesDir + "rollingoff.png");
            sprRolling.setPosition(cc.p(winSize.width/2 - 115 + temp,  winSize.height -770));
            guideLayer.addChild(sprRolling);
            arrRolling.push(sprRolling);
        } 
        
        setPageNumber();

        var sprGuide = cc.Sprite.create(resourcesLDir + "guide1.png");
        sprGuide.setPosition(cc.p(winSize.width/2,  winSize.height -960));
        guideLayer.addChild(sprGuide);
        sprGuide.setTag(11);

        var startImage = new cc.MenuItemImage(resourcesLDir + "startbtn.png",resourcesLDir + "startbtn.png", finishGuide);
        startImage.setPosition(cc.p(winSize.width/2,winSize.height- 1200));
        var menuStart = new cc.Menu(startImage);
        menuStart.setPosition(cc.p(0,0));
        menuStart.setTag(12);
        guideLayer.addChild(menuStart);  
        menuStart.setVisible(false);

    }
    function finishGuide(){
        main.removeChild(guideLayer);
        initMainUI();
        isSwipe = true;
        setMoveContextbg(true);
    }
    function setPageNumber(){

        for (var i = 0; i < arrRolling.length; i++){
            var spr = arrRolling[i];
            var fileName = "rollingoff.png";
            if (i == step-1) fileName = "rollingon.png";

            spr.setTexture(resourcesDir + fileName);
        }
    }
    function initCopyright(){
        var lbCopyright = cc.LabelTTF.create("Copyright ⓒ 2020, Johnson & Johnson Korea Ltd, All rights reserved.", "Helvetica", 20);
        lbCopyright.setPosition(cc.p(winSize.width/2,50));
        lbCopyright.setColor(cc.color(255, 255, 255));
        popupLayer.addChild(lbCopyright);

        var lbCcp = cc.LabelTTF.create("KR_2019_469", "Helvetica", 20);
        lbCcp.setPosition(cc.p(winSize.width/2 + 260,20));
        lbCcp.setColor(cc.color(255, 255, 255));
        main.addChild(lbCcp,2);
    }
    /**
     * 스타트 팝업
     * */
    function initStartPopup() {

        popupLayer = new cc.LayerColor(cc.color(255,255,255,255));
        popupLayer.setPosition(cc.p(0 /2,0));
        main.addChild(popupLayer,2);

        var bg = cc.Sprite.create(resourcesLDir + "part2bg.png");
        bg.setPosition(cc.p(winSize.width/2,  winSize.height/2));
        popupLayer.addChild(bg);

        var startImage = new cc.MenuItemImage(resourcesDir + "start2.png",resourcesDir + "start2.png", startFunc);
        startImage.setPosition(cc.p(winSize.width/2,winSize.height/2+150));
        var menuStart = new cc.Menu(startImage);
        menuStart.setPosition(cc.p(0,0));
        popupLayer.addChild(menuStart);   
     }
     var startFunc = function() {
        touchState = true;
        main.removeChild(popupLayer);
    }
    function setSwipe(isLeft){

        if (touchState == false) return;
        
        if (isLeft == true) {
            if (step <= 1) step = 1;
            else step--;
        } else {
            if (step >= 3) step = 3;
            else step++;
        }

        var spr = guideLayer.getChildByTag(11);
        spr.setTexture(resourcesLDir + "guide" + step + ".png");

        var sprMan = guideLayer.getChildByTag(20);
        sprMan.setTexture(resourcesDir + "man_" + step + ".png");

        var startBtn = guideLayer.getChildByTag(12);
        
        if(step == 3) startBtn.setVisible(true);
        else startBtn.setVisible(false);
        
        setPageNumber();
    }

    function initMainUI(){

        touchState = true;
        step = 1;

        popupLayer = new cc.LayerColor(cc.color(2,4,63,255));
        popupLayer.setPosition(cc.p(0 /2,0));
        main.addChild(popupLayer);

        sprBg = cc.Sprite.create(resourcesDir + "bg_1.png");
        sprBg.setPosition(cc.p(0,  winSize.height));
        sprBg.setAnchorPoint(0,1);
        popupLayer.addChild(sprBg);

        var sprCarbg = cc.Sprite.create(resourcesDir + "car_bg.png");
        sprCarbg.setPosition(cc.p(0,  winSize.height - 248));
        sprCarbg.setAnchorPoint(0,1);
        popupLayer.addChild(sprCarbg);

        var sprStepbg = cc.Sprite.create(resourcesDir + "step_1.png");
        sprStepbg.setPosition(cc.p(winSize.width - 94,  winSize.height/2 - 94));
        popupLayer.addChild(sprStepbg);
        sprStepbg.setTag(11);

        arrow = cc.Sprite.create(resourcesDir + "arrow.png");
        arrow.setPosition(cc.p(winSize.width/2, winSize.height/2 - 83));
        arrow.setAnchorPoint(0.8,0.5);
        popupLayer.addChild(arrow);
        arrow.setFlippedX(true);
        arrow.setTag(300);

        var sptTest1 = cc.Sprite.create(resourcesDir + "box_n.png");
        sptTest1.setPosition(cc.p(170, 10));
        arrow.addChild(sptTest1);
        sptTest1.setRotation(-90);
        sptTest1.setTag(200);
        sptTest1.setVisible(false);

        lbAngel = cc.LabelTTF.create("0", "Noto Sans KR", 40);
        lbAngel.setPosition(cc.p(winSize.width/2,winSize.height/2 - 280));
        lbAngel.setColor(cc.color(73, 153, 255));
        lbAngel.setAnchorPoint(1.0,0.5);
        popupLayer.addChild(lbAngel);

        targetPositionX = arrow.getPosition().x;
        targetPositionY = arrow.getPosition().y;
        
        sprContextbg = cc.Sprite.create(resourcesDir + "textbox_bg.png");
        sprContextbg.setPosition(cc.p(winSize.width/2,  winSize.height/2 - 804));
        popupLayer.addChild(sprContextbg);

        sprContext = cc.Sprite.create(resourcesLDir + "textbox_tt_1.png");
        sprContext.setPosition(cc.p(winSize.width/2 - 118, 120));
        sprContextbg.addChild(sprContext);

        var yesImage = new cc.MenuItemImage(resourcesLDir + "btn_yes.png",resourcesLDir + "btn_yes.png", buttonFunc); 
        var yesMenu = new cc.Menu(yesImage);
        yesMenu.setPosition(cc.p(winSize.width/2 + 220,164));
        yesImage.setTag(52);
        yesMenu.setTag(52);
        sprContextbg.addChild(yesMenu);
        yesMenu.setVisible(false);

        var noImage = new cc.MenuItemImage(resourcesLDir + "btn_no.png",resourcesLDir + "btn_no.png", buttonFunc); 
        var noMenu = new cc.Menu(noImage);
        noMenu.setPosition(cc.p(winSize.width/2 + 220,76));
        sprContextbg.addChild(noMenu);
        noImage.setTag(51);
        noMenu.setTag(51);
        noMenu.setVisible(false);

        var nextImage = new cc.MenuItemImage(resourcesLDir + "btn_next.png",resourcesLDir + "btn_next.png", buttonFunc); 
        var nextMenu = new cc.Menu(nextImage);
        nextMenu.setPosition(cc.p(winSize.width/2 + 220,122));
        sprContextbg.addChild(nextMenu);
        nextImage.setTag(50);
        nextMenu.setTag(50);
        nextMenu.setVisible(false);

        arrBtn.push(yesMenu);
        arrBtn.push(noMenu);
        arrBtn.push(nextMenu);

        setButtonVisible();

    }
    function buttonFunc(sender){
        if (touchState == false) return;

        var buttonIndex = sender.getTag();
        var yes = 52;
        var no = 51;
        var next = 50;

        // 텍스트 결과 저장
        setClickResult(buttonIndex);

        if (buttonIndex == yes) {
            step++;
        } else if(buttonIndex == no) {
            if (step <= 3) step = 4;
            else step = 8;
        } else {
            step++;
        }

        // 4 & 8 로딩
        if (step == 4 || step == 8){
            initLodingView();
        } else {
            setButtonVisible();
            setGuideText();
            setStep();
        }

        // 눈금터치 활성화
        if (step == 2 || step == 6){
            arrowState = true;
            setNotiAnimation(arrow);
        } else {
            arrowState = false;
        }

        // 테스트 이미지
        if (step == 3 || step == 7){
            setTestImgVisible(true);
        } else {
            setTestImgVisible(false);
        }

        setBGAnimation();
        

    }
    function setButtonPosition(){

        for(var i = 0; i < arrBtn.length; i++){
            var btn = arrBtn[i];
            if (btn.getTag() == 50) btn.setPosition(cc.p(winSize.width/2 - 275,122));
            else if (btn.getTag() == 51) btn.setPosition(cc.p(winSize.width/2 - 275,76));
            else btn.setPosition(cc.p(winSize.width/2 - 275,164));
        }
        sprContext.setPosition(cc.p(winSize.width/2 + 70, 120));
    }
    function setClickResult(index){

        var selectInfo = new structInfo();
        selectInfo.step = step;
        if (step == 2 || step == 6) {
            
            selectInfo.angle = angleResult;
        } else {
            selectInfo.selectIndex = index;
        }

        arrResult.push(selectInfo);
    }
    function structInfo(){
        var step = 0;
        var selectIndex = 0;
        var angle = 0;
        
      }
    function setTestImgVisible(isVisible){
        var arrowImg = popupLayer.getChildByTag(300);
        var test1 = arrowImg.getChildByTag(200);
        test1.setVisible(isVisible);

        if (isVisible == true) setNotiAnimation(test1);
    }
    function setMoveContextbg(isIN){

        var y = (isIN == true)? winSize.height/2 - 504 : winSize.height/2 - 804;
        var time = (isIN == true) ? 0.5 : 0.01;

        var moveTo = cc.MoveTo.create(time, cc.p(winSize.width/2, y));
        sprContextbg.runAction(moveTo);
    }
    function setButtonVisible(){

        var yes = 0;
        var no = 1;
        var next = 2;

        for(var i = 0; i < arrBtn.length; i++){
            var button = arrBtn[i];
            button.setVisible(false);
            
            if (step == 2 || step == 6) {
                if (i == next) button.setVisible(true);
            } else {
                if (i == yes || i == no) button.setVisible(true);
            } 
        }
    }
    function setBGAnimation(){
        var index = step;
        if(step == 4 || step == 8) return;
        if (step >= 5) index--;

        sprBg.setTexture(resourcesDir + "bg_" + index + ".png");
        // var animFrames = new Array();
        // for (var i = 1; i <= 3; i++) {
        //    var frame = cc.SpriteFrame.create(resourcesDir + "bg_" + i + ".png", cc.rect(0, 0, 750, 283));
        //    animFrames.push(frame);
        // }
        // var animation = cc.Animation.create(animFrames);
        // animation.setDelayPerUnit(0.2);
        // var action = cc.Animate.create(animation);
        // var repeat = cc.Repeat.create(action,1);
        // sprBg.runAction(repeat); 
    }
    function setGuideText(){

        if (step == 4 || step == 8) return;

        sprContext.setTexture(resourcesLDir + "textbox_tt_" + step + ".png");
    }
    function setStep() {
        if (step == 4 || step == 8) return;

        var spr = popupLayer.getChildByTag(11);
        spr.setTexture(resourcesDir + "step_" + step + ".png");

    }
    function initLodingView(){
        
        touchState = false;
        setMoveContextbg(false);

        var lodingText = (step == 4)? "lodingtext_0.png" : "lodingtext_1.png";

        guideLayer = new cc.LayerColor(cc.color(0,0,0,200));
        guideLayer.setPosition(cc.p(0,0));
        main.addChild(guideLayer,5);

        var sprText = cc.Sprite.create(resourcesLDir + lodingText);
        sprText.setPosition(cc.p(winSize.width/2,winSize.height/2 + 90));
        guideLayer.addChild(sprText);

        var sprLoding = cc.Sprite.create(resourcesDir + "loding.png");
        sprLoding.setPosition(cc.p(winSize.width/2,winSize.height/2 - 150));
        guideLayer.addChild(sprLoding);

        var sequence = cc.sequence([
            cc.RotateTo.create(3, 360*3),
            cc.callFunc(function () { 

                arrow.setRotation(0);
                lbAngel.setString("0");
                setButtonPosition();
                nextStep();
            }, main)
            ]);
        
        sprLoding.runAction(sequence);
    }
    function nextStep(){
        touchState = true;
        main.removeChild(guideLayer);

        if (step == 8) {
            initResult();
        } else {
            step = 5;
            setBGAnimation();
            setButtonVisible();
            setGuideText();
            setStep();
            setMoveContextbg(true);
        }
    }
    function initResult(){
        touchState = false;

        var scrollViewWidth = 687;
        var scrollViewHeight = 730;
        
        popupLayer = new cc.LayerColor(cc.color(2,4,63,255));
        popupLayer.setPosition(cc.p(0,0));
        main.addChild(popupLayer,2);

        var sprTitle = cc.Sprite.create(resourcesLDir + "resulttitle.png");
        sprTitle.setPosition(cc.p(winSize.width/2,winSize.height - 110));
        popupLayer.addChild(sprTitle);

        var sprRight = cc.Sprite.create(resourcesLDir + "right.png");
        sprRight.setPosition(cc.p(winSize.width/2,winSize.height - 250));
        popupLayer.addChild(sprRight);

        var sprRightText = cc.Sprite.create(getResultText(true));
        sprRightText.setPosition(cc.p(winSize.width/2,winSize.height - 310));
        popupLayer.addChild(sprRightText);

        var sprLeft = cc.Sprite.create(resourcesLDir + "left.png");
        sprLeft.setPosition(cc.p(winSize.width/2,winSize.height - 400));
        popupLayer.addChild(sprLeft);

        var sprLeftText = cc.Sprite.create(getResultText(false));
        sprLeftText.setPosition(cc.p(winSize.width/2,winSize.height - 460));
        popupLayer.addChild(sprLeftText);

        var retryImage = new cc.MenuItemImage(resourcesLDir + "btn_return.png",resourcesLDir + "btn_return.png", menuCallback);
        retryImage.setPosition(cc.p(105,winSize.height/2 - 550));
        retryImage.setTag(500);
        var retryMenu = new cc.Menu(retryImage);
        retryMenu.setPosition(cc.p(0,0));
        popupLayer.addChild(retryMenu);   

        var appstartImage = new cc.MenuItemImage(resourcesLDir + "btn_app.png",resourcesLDir + "btn_app.png", menuCallback);
        appstartImage.setPosition(cc.p(winSize.width/2,winSize.height/2 - 550));
        appstartImage.setTag(501);
        var appstartMenu = new cc.Menu(appstartImage);
        appstartMenu.setPosition(cc.p(0,0));
        popupLayer.addChild(appstartMenu);  

        var sociallinkImage = new cc.MenuItemImage(resourcesLDir + "btn_sns.png",resourcesLDir + "btn_sns.png", menuCallback);
        sociallinkImage.setPosition(cc.p(winSize.width/2 + 270,winSize.height/2 - 550));
        sociallinkImage.setTag(502);
        var sociallinkMenu = new cc.Menu(sociallinkImage);
        sociallinkMenu.setPosition(cc.p(0,0));
        popupLayer.addChild(sociallinkMenu); 

        initScrollView();
        
        var sprBG = cc.Sprite.create(resourcesDir + "gray_box.png");
        sprBG.setPosition(cc.p(scrollViewWidth / 2, scrollViewHeight + 570));
        sprBG.setAnchorPoint(cc.p(0.5, 1.0));
        scrollView.addChild(sprBG);

        var sprContextTitle = cc.Sprite.create(resourcesLDir + "result_context_title.png");
        sprContextTitle.setPosition(cc.p(scrollViewWidth / 2 - 15, 950));
        sprBG.addChild(sprContextTitle);

        var sprGuideBG = cc.Sprite.create(resourcesDir + "img_cut.png");
        sprGuideBG.setPosition(cc.p(scrollViewWidth / 2 - 15, 655));
        sprBG.addChild(sprGuideBG);

        var sprExmple = cc.Sprite.create(resourcesLDir + "exmple.png");
        sprExmple.setPosition(cc.p(480, 450));
        sprGuideBG.addChild(sprExmple);

        var sprLine_all = cc.Sprite.create(resourcesDir + "line_all.png");
        sprLine_all.setPosition(cc.p(280, 310));
        sprGuideBG.addChild(sprLine_all);
        sprLine_all.setScale(0.77);

        var sprArrow = cc.Sprite.create(resourcesDir + "arrow.png");
        sprArrow.setPosition(cc.p(280, 225));
        sprGuideBG.addChild(sprArrow);
        sprArrow.setAnchorPoint(0.8,0.5);
        sprArrow.setFlippedX(true);
        sprArrow.setScale(0.7);
        sprArrow.setRotation(120);

        var sptBox = cc.Sprite.create(resourcesDir + "box_b.png");
        sptBox.setPosition(cc.p(170, 10));
        sprArrow.addChild(sptBox);
        sptBox.setRotation(-90);
        setFadeInOut(sptBox);

        var sprLine = cc.Sprite.create(resourcesDir + "line.png");
        sprLine.setPosition(cc.p(-82, 13));
        sprArrow.addChild(sprLine);
        sprLine.setScaleX(1.42);
        sprLine.setScaleY(1.7);
        setFadeInOut(sprLine);

        var lbAngel = cc.LabelTTF.create("120", "Noto Sans KR", 40);
        lbAngel.setPosition(cc.p(277,74));
        lbAngel.setColor(cc.color(73, 153, 255));
        lbAngel.setAnchorPoint(1.0,0.5);
        sprGuideBG.addChild(lbAngel);

        var sprContext = cc.Sprite.create(resourcesLDir + "result_guide_text_1.png");
        sprContext.setPosition(cc.p(scrollViewWidth / 2 - 65, -220));
        sprGuideBG.addChild(sprContext);
        
        var sprGuideText2 = cc.Sprite.create(resourcesLDir + "result_guide_text_2.png");
        sprGuideText2.setPosition(cc.p(scrollViewWidth / 2, scrollViewHeight - 490));
        sprGuideText2.setAnchorPoint(cc.p(0.5, 1.0));
        scrollView.addChild(sprGuideText2);

        scrollView.setInnerContainerSize(cc.size(scrollViewWidth,1320));

        setResultArrow(sprArrow, lbAngel);

    }
    function initScrollView(){

        if (scrollView != null) {
            popupLayer.removeChild(scrollView);
            scrollView = null;
        }

        scrollView = new ccui.ScrollView();
        scrollView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        scrollView.setTouchEnabled(true);
        scrollView.setBounceEnabled(true);
        scrollView.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        scrollView.setBackGroundColor(cc.color(34,34,34));
        scrollView.setContentSize(cc.size(687, 570));
        scrollView.setAnchorPoint(cc.p(0.5, 0.5));
        scrollView.setBackGroundImage(resourcesDir + "white_box.png");
        scrollView.setPosition(cc.p(winSize.width/2, winSize.height - 810));
        popupLayer.addChild(scrollView);
    }
    function setResultArrow(arrow, lbAngel){
        
        for (var i = 0; i < arrResult.length; i++) {
            var info = arrResult[i];
            if (info.step == 6) {
                arrow.setRotation(info.angle);
                var number = parseInt(info.angle);
                lbAngel.setString(String(number));
            }
            else if (info.step == 2) {
                arrow.setRotation(info.angle);
                var number = parseInt(info.angle);
                lbAngel.setString(String(number));
            }
        }
    }
    function getResultText(isRight){
        
        var no = resourcesLDir + "resulttext_2.png"; //51
        var yes = resourcesLDir + "resulttext_3.png"; //52
        var pass = resourcesLDir + "resulttext_1.png"; 
        var fileName = pass;
        
        if (isRight == true){
            for (var i = 0; i < arrResult.length; i++){
                var info = arrResult[i];

                if(info.step == 3){ 
                    if (info.selectIndex == 51) fileName = no;
                    if (info.selectIndex == 52) fileName = yes;
                } else if (info.step == 1) {
                    if (info.selectIndex == 51) fileName = pass;
                }
            }
        } else {
            for (var i = 0; i < arrResult.length; i++){
                var info = arrResult[i];
                if(info.step == 7){ 
                    if (info.selectIndex == 51) fileName = no;
                    if (info.selectIndex == 52) fileName = yes;
                } else if (info.step == 5) {
                    if (info.selectIndex == 51) fileName = pass;
                }
            }
        }
        return fileName;
    }
    function menuCallback(sender) {
        var buttonIndex = sender.getTag();
        var retry = 500;
        var app = 501;
        var sns = 502;

        if (isPause == true) return;

        if (buttonIndex == retry){
            retryCallback();
        } else if (buttonIndex == app) {
            appLinkCallback();
        } else {
            socialLinkCallback();
        }

    }
    function retryCallback(){
        main.removeChild(popupLayer);

        step = 1;
        touchState = false;
        arrowState = false;
        isSwipe = false;
        arrBtn = [];
        arrResult = [];
        angleResult = 0;

        initMainUI();
        isSwipe = true;
        setMoveContextbg(true);
    }
    function appLinkCallback(){
        window.location.href = 'https://my.acuvue.co.kr/view/consumer/forwardAppUniversal.jsp';
    }
    function socialLinkCallback(){
        
        isPause = true;
        scrollView.setTouchEnabled(false);

        var socialLinkLayer = new cc.LayerColor(cc.color(0,0,0,100));
        socialLinkLayer.setPosition(cc.p(0 ,0));
        main.addChild(socialLinkLayer,3);
        socialLinkLayer.setTag(111);

        var sprSocialPopup = cc.Sprite.create(resourcesLDir + "result_sns_bg.png");
        sprSocialPopup.setPosition(cc.p(winSize.width/2, winSize.height/2));
        socialLinkLayer.addChild(sprSocialPopup);

        var kakaoImage = new cc.MenuItemImage(resourcesDir + "kakao2.png",resourcesDir + "kakao2.png", snsCallback);
        kakaoImage.setTag(11);
        var kakaoMenu = new cc.Menu(kakaoImage);
        kakaoMenu.setPosition(cc.p(winSize.width/2,616));
        socialLinkLayer.addChild(kakaoMenu);   

        var facebookImage = new cc.MenuItemImage(resourcesDir + "fb2.png",resourcesDir + "fb2.png", snsCallback);
        facebookImage.setTag(12);
        var facebookMenu = new cc.Menu(facebookImage);
        facebookMenu.setPosition(cc.p(winSize.width/2,490));
        socialLinkLayer.addChild(facebookMenu); 

        var closeImage = new cc.MenuItemImage(resourcesDir + "x.png",resourcesDir + "x.png", snsCallback);
        closeImage.setTag(13);
        var closeMenu = new cc.Menu(closeImage);
        closeMenu.setPosition(cc.p(winSize.width/2 + 265,910));
        socialLinkLayer.addChild(closeMenu); 
    }
    function snsCallback(sender){
        var buttonIndex = sender.getTag();
        var kakao = 11;
        var facebook = 12;
        var close = 13;

        if (buttonIndex == kakao) {
            kakaoCallback();
        } else if (buttonIndex == facebook) {
            facebookCallback();
        } else {
            snsCloseCallback();
        }

    }
    function kakaoCallback(){
        
        var linkURL = snsLink;
        Kakao.init(kakaoTalkKey);
        Kakao.Link.sendDefault({
            objectType: 'text',
            text: 'Fan & Block',
            installTalk: true,
            link: {
                webUrl: linkURL,
                mobileWebUrl: linkURL
            },
            buttons: [{
            title: '자세히 보기',
            link: {
                webUrl: linkURL,
                mobileWebUrl: linkURL
            }
            }],
        callback: function(data) {
             // 추천이력 등록
             //sendRecommendSNS("kakao");
        }
        });
     }
     function facebookCallback(){
        FB.ui({
            method: 'share',
            href: snsLink
          }, function(response){});
        
     }
     function snsCloseCallback(){
        
        var socialLinkLayer = main.getChildByTag(111);

        if (socialLinkLayer != null) {
            main.removeChild(socialLinkLayer);
            socialLinkLayer = null;
            isPause = false;
            scrollView.setTouchEnabled(true);
        }

     }

     /**
     * 각도 값 반환
     * */
    function getAngle(location) {
        
        var r = Math.atan2(location.x - targetPositionX , location.y - targetPositionY);
        var angle = 0;         
        if (r < 0) {
            r += Math.PI *2;
            angle = r * 180 / Math.PI;
            while (angle < 0) {
                angle += 360;
            }
            
        } else {
            angle = r * 180 / Math.PI;
        }

        angle += 90;

        if (angle >= 360) angle -= 360;


        return angle;
    }
    function setArrowAngle(touch, event){

        if (arrowState == false) return;
 
        var target = event.getCurrentTarget();
        var location = target.convertToNodeSpace(touch.getLocation());
        var angle = getAngle(location);
        
        if (angle >= 0 && angle <= 181){
            arrow.setRotation(angle);
            var number = parseInt(angle);
            lbAngel.setString(String(number));

            var temp =  angle % 10;
            // 나머지를 구해 반올림 반내림 처리
            if (temp >= 5) angleResult = angle - temp + 10;
            else angleResult = angle - temp;
        }
    }
    function setNotiAnimation(target){
        var scaleUp = cc.scaleTo(0.5, 1.2, 1.2);
        var scaleDown = cc.scaleTo(0.1, 1.0, 1.0);

        var sequence = cc.sequence([
            scaleUp,
            scaleDown
            ]);
        var rep = cc.Repeat.create(sequence, 2);

        target.runAction(rep);
    }
    function setFadeInOut(target){
        var fadeIn = cc.FadeIn.create(1.0);
        var fadeOut = cc.FadeOut.create(1.0);
        var sequence = cc.sequence([
            fadeIn,
            fadeOut
            ]).repeatForever();
            target.runAction(sequence);

    }

};

